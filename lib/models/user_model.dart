class User {
    final String id;
    final String name;
    final String email;
    final String position;

    User({ 
        required this.id, 
        required this.name, 
        required this.email,
        required this.position
    });
}