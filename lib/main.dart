import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
// ignore: import_of_legacy_library_into_null_safe
// import 'package:time_machine/time_machine.dart';

// For personal imports
import './models/navigation_model.dart';
import './providers/navigation_provider.dart';
import './screens/account_screen.dart';
import './screens/add_zeiterfassung_screen.dart';
import './screens/login_screen.dart';
import './screens/tabs_screen.dart';
import './screens/time_tracking_screen.dart';

void main() async {
    Navigation initialItem = Navigation.meinkonto;

    // WidgetsFlutterBinding.ensureInitialized();
    // await TimeMachine.initialize({'rootBundle': rootBundle});

    runApp(MyApp(initialItem));
} 

class MyApp extends StatelessWidget {
    final Navigation _navigationItem;

    MyApp(this._navigationItem);

    @override
    Widget build(BuildContext context) {
        return ChangeNotifierProvider(
            create: (BuildContext context) => NavigationProvider(_navigationItem),
            child: MaterialApp(
                debugShowCheckedModeBanner: false,
                title: 'Time Tracking App',
                theme: ThemeData(
                    primaryColor: Colors.white,
                    textTheme: ThemeData.light().textTheme.copyWith(
                        bodyText2: TextStyle(
                            fontSize: 16,
                            fontFamily: 'Roboto',
                        ),
                        subtitle2: TextStyle(
                            fontFamily: 'AllertaStencil',
                        ),
                        headline5: TextStyle(
                            fontFamily: 'Mulish',
                            fontSize: 15,
                            color: Colors.grey.shade600
                        ),
                        headline6: TextStyle(
                            fontSize: 15,
                            fontFamily: 'Mulish',
                        ),
                    ),
                ),
                initialRoute: LoginScreen.routeName,
                routes: {
                    LoginScreen.routeName: (context) => LoginScreen(),
                    AccountScreen.routeName: (context) => AccountScreen(),
                    TabsScreen.routeName: (context) => TabsScreen(),
                    TimeTrackingScreen.routeName: (context) => TimeTrackingScreen(),
                    AddZeiterfassungScreen.routeName: (context) => AddZeiterfassungScreen(),
                },
            ),
        );
    }
}