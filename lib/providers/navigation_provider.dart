import 'package:flutter/material.dart';

// For personal imports
import '../models/navigation_model.dart';

class NavigationProvider extends ChangeNotifier {
    Navigation _navigationTile = Navigation.meinkonto;

    NavigationProvider(this._navigationTile);
    
    Navigation get navigationTile => _navigationTile;

    setNavigationTile(Navigation item) {
        _navigationTile = item;
        notifyListeners();
    }
}