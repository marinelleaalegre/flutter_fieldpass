import 'package:flutter/material.dart';

// For personal imports
import '../models/user_model.dart';

class UserProvider extends ChangeNotifier {
    List<User> DUMMY_USERS =  [
        User(
            id: '1', 
            name: 'Greg Neu', 
            email: 'max.mustermann@bkl.de',
            position: 'Monteur'
        ),
        User(
            id: '2', 
            name: 'Ingo Flamingo', 
            email: 'ingo.flamingo@f-bootcamp.com',
            position: 'Ansprechpartner'
        ),
        User(
            id: '3', 
            name: 'Andero Mustermann', 
            email: 'andero.mustermann@f-bootcamp',
            position: 'Abteilungsleiter'
        ),
    ];

    List<User> get users {
        return [...DUMMY_USERS];
    }

}