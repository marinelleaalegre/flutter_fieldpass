import 'package:flutter/material.dart';

Widget titleHeader(String title) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        padding: EdgeInsets.all(10),
        child: Text(
            title,
            style: TextStyle(
                fontFamily: 'AllertaStencil',
                fontSize: 25
            ),
        )
    );
}

Widget titleHeadings(String title){
    return Container(
        child: Text(
            title,
            style: TextStyle(
                fontFamily: 'AllertaStencil',
                fontSize: 23
            ),
        )
    );
}