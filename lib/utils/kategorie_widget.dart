import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget kategorieField(TextEditingController txtController, FocusScopeNode focusNode, Function tapHandler) {
    return Container(
        margin: EdgeInsets.only(bottom: 20),
        child: TextFormField(
            decoration: InputDecoration(
                labelText: 'Wahlen Sie bitte Kategorie aus',
                labelStyle: TextStyle(
                    color: Colors.grey.shade400,
                    fontFamily: 'Mulish'
                ),
                enabledBorder: UnderlineInputBorder(      
                    borderSide: BorderSide(color: Colors.grey.shade400),   
                ),  
                focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey.shade400),
                ),
                suffixIcon: IconButton(
                    onPressed: () => tapHandler(), 
                    icon: Icon(FontAwesomeIcons.angleDoubleDown),
                    color: Colors.black54,
                )
            ),
            cursorColor: Colors.grey.shade400,
            keyboardType: TextInputType.text,
            controller: txtController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'This is required.';
            },
        ),
    );
}