import 'package:flutter/material.dart';

Widget buttonPairs(Function cancel, Function onSubmit) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
            Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: TextButton(
                    onPressed: () => cancel(), 
                    child: Text(
                        'Abbrechen',
                        style: TextStyle(
                            color: Colors.black
                        ),
                    )
                    
                ),
            ),
            ElevatedButton(
                onPressed: () => onSubmit(), 
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                        Text(
                            'Speichern ',
                            style: TextStyle(
                                color: Colors.white
                            ),
                        ),
                        SizedBox(width: 3,),
                        Image.asset('./assets/images/send.png'),
                        
                    ],
                ),
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
                ),
            ),
        ],
    );
}