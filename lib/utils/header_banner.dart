import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget headerBanner(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
            Padding(
                padding: EdgeInsets.all(20),
                child: InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Icon(FontAwesomeIcons.times),
                ),
            ),
            Padding(
                padding: EdgeInsets.all(20),
                child: Image.asset(
                    './assets/images/logo.png',
                    height: 40,
                    width: 50,
                ),
            )
        ],
    );
}