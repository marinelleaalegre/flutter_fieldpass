import 'package:flutter/material.dart';

Widget commentField(TextEditingController textController) {
    return Container(
        margin: EdgeInsets.only(bottom: 20),
        child: TextField(
            decoration: InputDecoration(
                hintText: 'Bemerkung hinzufügen',
                hintStyle: TextStyle(
                    color: Colors.grey.shade400,
                    fontFamily: 'Mulish'
                ),
                prefixIcon: Padding(
                    padding: const EdgeInsets.fromLTRB(10, 10, 10, 80), 
                    child: Image.asset(
                        './assets/images/ava.png',
                        height: 50,
                    ), 
                ),
                filled: true,
                border: InputBorder.none
            ),
            cursorColor: Colors.grey.shade400,
            keyboardType: TextInputType.multiline,
            controller: textController,
            maxLines: null,
            minLines: 4,
            textAlignVertical: TextAlignVertical.top
        )
    );
}