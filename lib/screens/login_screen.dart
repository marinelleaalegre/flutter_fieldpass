import 'package:flutter/material.dart';

// For personal imports
import './account_screen.dart';

class LoginScreen extends StatelessWidget {
    static const routeName = '/';

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                        Container(
                            padding: EdgeInsets.only(top: 130),
                            child: Image.asset('./assets/images/logo.png')
                        ),
                        Container(
                            padding: EdgeInsets.only(top: 50),
                            child: Text(
                                'Flutter FieldPass',
                                style: Theme.of(context).textTheme.bodyText2,
                            ),
                        ),
                        Container(
                            margin: EdgeInsets.fromLTRB(37, 65, 36, 230), //37 56 36 230
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        width: 4,
                                        color: Colors.grey.shade300
                                    )
                                )
                            ),
                            child: ListTile(
                                leading: Image.asset('./assets/images/microsoft-logo.png'),
                                title: Text(
                                    'Sign in with Microsoft',
                                    style: Theme.of(context).textTheme.headline6,
                                ),
                                trailing: Image.asset('./assets/images/icon_arrow_double.png'),
                                onTap: () => Navigator.pushNamed(context, AccountScreen.routeName),
                            ),
                        ),
                        Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                                margin: EdgeInsets.only(left: 50, right: 50),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                        Text(
                                            'Impressum',
                                            style: Theme.of(context).textTheme.headline6,
                                        ),
                                        Text(
                                            'Datenschutz',
                                            style: Theme.of(context).textTheme.headline6,
                                        ),
                                    ],
                                ),
                            ),
                        )
                    ],
                ),
            ),
        );
    }
}