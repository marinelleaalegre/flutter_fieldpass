import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

// For personal imports
import '../screens/add_zeiterfassung_screen.dart';
import '../widgets/app_drawer.dart';

class TimeTrackingScreen extends StatelessWidget {
    static const routeName = '/zeiterfassung';

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                leadingWidth: 25,
                title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        Text(
                            'Donnerstag',
                            style: TextStyle(
                                fontFamily: 'AllertaStencil',
                                fontSize: 20,
                            ),
                        ),
                        Row(
                            children: [
                                Image.asset('./assets/images/status.png'),
                                Text(
                                    '12.01.2021',
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontFamily: 'Mulish'
                                    ),
                                )
                            ],
                        )
                    ],
                ),
                actions: [
                    IconButton(
                        iconSize: 40,
                        onPressed: () { }, 
                        icon: Image.asset(
                            './assets/images/button.png',
                        )
                    ),
                    IconButton(
                        iconSize: 50,
                        onPressed: () => Navigator.pushNamed(context, AddZeiterfassungScreen.routeName), 
                        icon: Image.asset(
                            './assets/images/button1.png',
                        )
                    ),
                ],
                bottom: PreferredSize(
                    preferredSize: Size.fromHeight(80.0),
                    child: Container(
                        margin: EdgeInsets.all(5),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                                Column(
                                    children: [
                                        Container(
                                            height: 40,
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                elevation: 0,
                                                onPressed: () {},
                                                child: Icon(
                                                    FontAwesomeIcons.check,
                                                    color: Colors.black,
                                                    size: 19,
                                                ),
                                                backgroundColor: Colors.red,
                                            ),
                                        ),
                                        Text(
                                            'Mo',
                                            style: Theme.of(context).textTheme.subtitle2
                                        )
                                    ],
                                ),
                                Column(
                                    children: [
                                        Container(
                                            height: 40,
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                elevation: 0,
                                                onPressed: () {},
                                                child: Icon(
                                                    FontAwesomeIcons.check,
                                                    color: Colors.black,
                                                    size: 19,
                                                ),
                                                backgroundColor: Colors.amber,
                                            ),
                                        ),
                                        Text(
                                            'Di',
                                            style: Theme.of(context).textTheme.subtitle2
                                        )
                                    ],
                                ),
                                Column(
                                    children: [
                                        Container(
                                            height: 40,
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                elevation: 0,
                                                onPressed: () {},
                                                child: Icon(
                                                    FontAwesomeIcons.solidPaperPlane,
                                                    size: 19,
                                                ),
                                                backgroundColor: Colors.indigoAccent.shade100
                                            ),
                                        ),
                                        Text(
                                            'Mi',
                                            style: Theme.of(context).textTheme.subtitle2
                                        )
                                    ],
                                ),
                                Column(
                                    children: [
                                        Container(
                                            height: 40,
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                elevation: 0,
                                                onPressed: () {},
                                                child: Transform(
                                                    alignment: Alignment.center,
                                                    transform: Matrix4.rotationY(math.pi),
                                                    child: Icon(
                                                        FontAwesomeIcons.pen,
                                                        size: 19,
                                                    ),
                                                ),
                                                backgroundColor: Colors.deepPurple.shade300
                                            ),
                                        ),
                                        Text(
                                            'Do',
                                            style: TextStyle(
                                                color: Colors.deepPurple.shade300,
                                                fontFamily: 'AllertaStencil'
                                            )
                                        )
                                    ],
                                ),
                                Column(
                                    children: [
                                        Container(
                                            height: 40,
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                elevation: 0,
                                                onPressed: () {},
                                                child: Text(
                                                    'Fr',
                                                    style: TextStyle(
                                                        color: Colors.white
                                                    ),
                                                ),
                                                backgroundColor: Color.fromRGBO(230, 229, 220, 2),
                                            ),
                                        ),
                                        Text(
                                            'Fr',
                                            style: Theme.of(context).textTheme.subtitle2
                                        )
                                    ],
                                ),
                                Column(
                                    children: [
                                        Container(
                                            height: 40,
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                elevation: 0,
                                                onPressed: () {},
                                                child: Text(
                                                    'Sa',
                                                    style: TextStyle(
                                                        color: Colors.white
                                                    ),
                                                ),
                                                backgroundColor: Color.fromRGBO(230, 229, 220, 2),
                                            ),
                                        ),
                                        Text(
                                            'Sa',
                                            style: Theme.of(context).textTheme.subtitle2
                                        )
                                    ],
                                ),
                                Column(
                                    children: [
                                        Container(
                                            height: 40,
                                            margin: EdgeInsets.only(bottom: 5),
                                            child: FloatingActionButton(
                                                heroTag: null,
                                                elevation: 0,
                                                onPressed: () {},
                                                child: Text(
                                                    'So',
                                                    style: TextStyle(
                                                        color: Colors.white
                                                    ),
                                                ),
                                                backgroundColor: Color.fromRGBO(230, 229, 220, 2),
                                            ),
                                        ),
                                        Text(
                                            'So',
                                            style: Theme.of(context).textTheme.subtitle2
                                        )
                                    ],
                                ),
                            ],
                        )
                    ), 
                )
            ),
            drawer: AppDrawer(),
            body: Center(
                child: Text('Zeiterfassung TimeTable'),
            ),
        );
    }
}