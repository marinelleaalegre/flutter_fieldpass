import 'package:flutter/material.dart';

// For personal imports
import './add_arbeitszeit_screen.dart';
import './add_pause_screen.dart';
import '../utils/header_banner.dart';

class AddZeiterfassungScreen extends StatelessWidget {
    static const routeName = '/add-zeiterfassung';

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: DefaultTabController(
                length: 2, 
                child: Container(
                    margin: EdgeInsets.symmetric(vertical: 30),
                    color: Colors.white,
                    child: Column(
                        children: [
                            headerBanner(context),
                            Container(
                                // color: Colors.transparent,
                                padding: EdgeInsets.all(10),
                                child: TabBar(
                                    labelColor: Colors.black,
                                    unselectedLabelColor: Colors.grey,
                                    labelStyle: TextStyle(
                                        fontFamily: 'AllertaStencil',
                                        fontSize: 16
                                    ),
                                    indicator: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage('./assets/images/lines.png'),
                                            fit: BoxFit.cover
                                        ),
                                        border: Border(
                                            bottom: BorderSide(
                                                width: 4,
                                                color: Colors.deepPurple.shade400
                                            )
                                        ),
                                    ),
                                    tabs: [
                                        Tab(text: 'Arbeitszeit'),
                                        Tab(text: 'Pause'),
                                    ]
                                ),
                            ),
                            Expanded(
                                child: TabBarView(
                                    children: [
                                        AddArbeitszeitScreen(),
                                        AddPauseScreen(),
                                    ]
                                )
                            ),
                        ],
                    )
                ),
            )
        );
    }
}