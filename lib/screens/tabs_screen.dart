import 'package:flutter/material.dart';

// For personal imports
import '../widgets/app_drawer.dart';
import '../widgets/tab_view.dart';


class TabsScreen extends StatelessWidget {
    static const routeName = '/visitenkarte';

    @override
    Widget build(BuildContext context) {
        return DefaultTabController(
            length: 2, 
            child: Scaffold(
                appBar: AppBar(),
                drawer: AppDrawer(),
                body: Column(
                    children: [
                        Container(
                            color: Colors.transparent,
                            padding: EdgeInsets.all(15),
                            child: TabBar(
                                labelColor: Colors.black,
                                unselectedLabelColor: Colors.grey,
                                labelStyle: TextStyle(
                                    fontFamily: 'AllertaStencil',
                                    fontSize: 16
                                ),
                                indicator: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage('./assets/images/lines.png'),
                                        fit: BoxFit.cover
                                    ),
                                    border: Border(
                                        bottom: BorderSide(
                                            width: 4,
                                            color: Colors.deepPurple.shade400
                                        )
                                    ),
                                ),
                                tabs: [
                                    Tab(text: 'Meine Visitenkarte'),
                                    Tab(text: 'Vorgesetzte'),
                                ]
                            ),
                        ),
                        Expanded(
                            child: TabView()
                        ),
                    ],
                )
            )
        );
    }
}