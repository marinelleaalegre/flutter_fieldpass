import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

// For personal imports
import '../utils/button_pair.dart';
import '../utils/kategorie_widget.dart';
import '../utils/text_area_field.dart';
import '../utils/title_header.dart';

class AddArbeitszeitScreen extends StatefulWidget {
    @override
    _AddArbeitszeitScreenState createState() => _AddArbeitszeitScreenState();
}

class _AddArbeitszeitScreenState extends State<AddArbeitszeitScreen> {
    final _formKey = GlobalKey<FormState>();
    final _kategorieController = TextEditingController();
    final _projektnummerController = TextEditingController();
    final _commentController = TextEditingController();
    late DateTime _startDateTime;
    late DateTime _endDateTime;
    // final String dropDownValue = ;

    Widget startTimePicker() {
        return Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            margin: EdgeInsets.all(1),
            child: Row(
                children: [
                    Transform.rotate(
                        angle: math.pi / 4.0,
                        child: Icon(
                            FontAwesomeIcons.solidPaperPlane,
                            size: 19,
                        ),
                    ),
                    TimePickerSpinner(
                        is24HourMode: false,
                        normalTextStyle: TextStyle(
                            color: Colors.grey.shade600,
                            fontFamily: 'AllertaStencil'
                        ),
                        highlightedTextStyle: TextStyle(
                            color: Colors.black,
                            fontFamily: 'AllertaStencil'
                        ),
                        isForce2Digits: true,
                        spacing: 5,
                        onTimeChange: (time) {
                            // print(time);
                            setState(() {
                                _startDateTime = time;
                            });
                        },
                    ),
                ],
            )
        );
    }

    Widget endTimePicker() {
        return Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            margin: EdgeInsets.all(1),
            child: Row(
                children: [
                    Icon(
                        FontAwesomeIcons.stop,
                        size: 19,
                    ),
                    TimePickerSpinner(
                        is24HourMode: false,
                        normalTextStyle: TextStyle(
                            color: Colors.grey.shade600,
                            fontFamily: 'AllertaStencil'
                        ),
                        highlightedTextStyle: TextStyle(
                            color: Colors.black,
                            fontFamily: 'AllertaStencil'
                        ),
                        isForce2Digits: true,
                        spacing: 5,
                        onTimeChange: (time) {
                            // print(time);
                            setState(() {
                                _endDateTime = time;
                            });
                        },
                    ),
                ],
            )
        );
    }
            
    @override
    Widget build(BuildContext context) {
        final FocusScopeNode focusNode = FocusScope.of(context);

        Widget projektnummerField = Container(
            margin: EdgeInsets.only(bottom: 20),
            child: TextFormField(
                decoration: InputDecoration(
                    labelText: 'Projektnummer hinzufügen',
                    labelStyle: TextStyle(
                        color: Colors.grey.shade400,
                        fontFamily: 'Mulish'
                    ),
                    enabledBorder: UnderlineInputBorder(      
                        borderSide: BorderSide(color: Colors.grey.shade400),   
                    ),  
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey.shade400),
                    ),
                    suffixIcon: IconButton(
                        onPressed: () {}, 
                        icon: Icon(FontAwesomeIcons.angleDoubleDown),
                        color: Colors.black54,
                    )
                ),
                cursorColor: Colors.grey.shade400,
                keyboardType: TextInputType.text,
                controller: _projektnummerController,
                onEditingComplete: focusNode.nextFocus,
                validator: (value) {
                    return (value != null && value.isNotEmpty) ? null : 'This is required.';
                },
            ),
        );

        Widget mitarbeiterField = Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        titleHeadings('Mitarbeiter'),
                        Text(
                            'hinzufügen oder bearbeiten',
                            style: TextStyle(
                                color: Colors.grey.shade400,
                                fontSize: 15,
                                fontFamily: 'Roboto'
                            ),
                        ),
                    ],
                ),
                Container(
                    height: 50,
                    child: FloatingActionButton(
                        heroTag: null,
                        onPressed: () {},
                        child: Icon(FontAwesomeIcons.plus),
                        backgroundColor: Colors.black,
                    ),
                )
            ],
        );

        Widget formAddArbeitszeit = Form(
            key: _formKey,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    titleHeadings('Kategorie'),
                    kategorieField(_kategorieController, focusNode, () {}),
                    titleHeadings('Projektnummer'),
                    projektnummerField,
                    mitarbeiterField,
                    SizedBox(height: 25,),
                    titleHeadings('Arbeitszeit'),
                    Row(
                        children: [
                            startTimePicker(),
                            endTimePicker()
                        ],
                    ),
                    SizedBox(height: 30,),
                    commentField(_commentController),
                ],
            ),
        );

        return Scaffold(
            body: SingleChildScrollView(
                child: Container(
                    color: Colors.white,
                    width: double.infinity,
                    padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
                    child: Column(
                        children: [
                            formAddArbeitszeit,
                            buttonPairs(() {}, () {}),
                        ],
                    )
                ),
            )
        );
    }
}