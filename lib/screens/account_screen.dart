import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

// For personal imports
import '../providers/user_provider.dart';
import '../utils/title_header.dart';
import '../widgets/app_drawer.dart';

class AccountScreen extends StatefulWidget {
    static const routeName = '/mein-konto';

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(),
            drawer: AppDrawer(),
            body: SingleChildScrollView(
                child: Container(
                    width: double.infinity,
                    margin: EdgeInsets.all(8),
                    padding: EdgeInsets.all(5),
                    child: Column(
                        children: [
                            Card(
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                        titleHeader('Mein Konto'),
                                        ListTile(
                                            leading: CircleAvatar(
                                                radius: 30,
                                                child: Container(
                                                    child: Image.asset(
                                                        './assets/images/ava.png',
                                                    ),
                                                )
                                            ),
                                            title: Text(
                                                'Greg Neu',
                                                style: TextStyle(
                                                    fontFamily: 'AllertaStencil',
                                                    fontSize: 20,
                                                ),
                                            ),
                                            subtitle: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                    Text(
                                                        'max.mustermann@bkl.de',
                                                        style: Theme.of(context).textTheme.headline5
                                                    ),
                                                    Container(
                                                        padding: EdgeInsets.only(top: 5),
                                                        child: Text(
                                                            'Monteur',
                                                            style: Theme.of(context).textTheme.headline5,
                                                        ),
                                                    ),
                                                ],
                                            )
                                        ),
                                        titleHeader('Ansprechpartner'),
                                        ListTile(
                                            leading: CircleAvatar(
                                                radius: 30,
                                                child: Image.asset(
                                                    './assets/images/ava1.png',
                                                ),
                                            ),
                                            title: Text(
                                                'Ingo Flamingo',
                                                style: TextStyle(
                                                    fontFamily: 'AllertaStencil',
                                                    fontSize: 20,
                                                ),
                                            ),
                                            subtitle: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                    Text(
                                                        'ingo.flamingo@f-bootcamp.com',
                                                        style: Theme.of(context).textTheme.headline5,
                                                    ),
                                                    Stack(
                                                        alignment: Alignment.center,
                                                        children: [
                                                            Image.asset(
                                                                './assets/images/bg.png',
                                                                fit: BoxFit.fill,
                                                            ),
                                                            Text(
                                                                '0610-123456789',
                                                                style: TextStyle(
                                                                    color: Colors.white,
                                                                    fontSize: 14
                                                                )
                                                            ),
                                                        ],
                                                    )
                                                ],
                                            )
                                        ),
                                        titleHeader('Wochenbericht'),
                                        ListTile(
                                            leading: Image.asset('./assets/images/kalender.png'),
                                            title: Text(
                                                '12.03 - 19.03.2021',
                                                style: TextStyle(
                                                    fontFamily: 'AllertaStencil',
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.bold
                                                ),
                                            ),
                                            subtitle: Container(
                                                padding: EdgeInsets.only(right: 10),
                                                child: ElevatedButton(
                                                    onPressed: () {},
                                                    child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                        children: [
                                                            Text('Wochenbericht zuschicken'),
                                                            Icon(
                                                                FontAwesomeIcons.solidPaperPlane,
                                                                size: 15,
                                                            )
                                                        ],
                                                    ),
                                                    style: ElevatedButton.styleFrom(
                                                        primary: Colors.black,
                                                        onPrimary: Colors.white,
                                                        textStyle: TextStyle(
                                                            fontSize: 15,
                                                            fontFamily: 'Roboto'
                                                        ),
                                                    ),
                                                ),
                                            )
                                        ),
                                        titleHeader('Monatsbericht'),
                                        ListTile(
                                            leading: Image.asset('./assets/images/kalender.png'),
                                            title: Text(
                                                'April 2020',
                                                style: TextStyle(
                                                    fontFamily: 'AllertaStencil',
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.bold
                                                ),
                                            ),
                                            subtitle: Container(
                                                alignment: Alignment.centerLeft,
                                                padding: EdgeInsets.only(bottom: 15),
                                                child: ElevatedButton(
                                                    onPressed: () {},
                                                    child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                        children: [
                                                            Text('Monatsbericht erstellen'),
                                                            Icon(
                                                                FontAwesomeIcons.solidPaperPlane,
                                                                size: 15,
                                                            )
                                                        ],
                                                    ),
                                                    style: ElevatedButton.styleFrom(
                                                        primary: Colors.black,
                                                        onPrimary: Colors.white,
                                                        textStyle: TextStyle(
                                                            fontSize: 15,
                                                            fontFamily: 'Roboto'
                                                        ),
                                                    ),
                                                ),
                                            )
                                        ),
                                    ],
                                ),
                            ),
                            Card(
                                color: Colors.grey.shade100,
                                elevation: 3,
                                child: Container(
                                    height: 70,
                                    padding: EdgeInsets.all(5),
                                    // margin: EdgeInsets.all(5),
                                    child: ListTile(
                                        selectedTileColor: Colors.grey.shade400,
                                        title: Text('Aktuellas Budget'),
                                        trailing: CircleAvatar(
                                            backgroundColor: Colors.amber.shade700,
                                            child: Container(
                                                margin: EdgeInsets.all(10),
                                                child: Text(
                                                    '7',
                                                    style: TextStyle(
                                                        color: Colors.white
                                                    ),
                                                ),
                                            )
                                        ),
                                        onTap: () {},
                                    ),
                                ),
                            ),
                            Card(
                                child: Container(
                                    width: double.infinity,
                                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                                    margin: EdgeInsets.only(bottom: 5),
                                    child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            titleHeader('Krankheitstage'),
                                            ListTile(
                                                leading: Text(
                                                    'Insgesamt',
                                                    style: Theme.of(context).textTheme.headline6
                                                ),
                                                trailing: Text(
                                                    '03',
                                                    style: TextStyle(
                                                        fontFamily: 'AllertaStencil',
                                                        fontSize: 18,
                                                        fontWeight: FontWeight.bold
                                                    ),
                                                ),
                                            ),
                                            Container(
                                                width: 200,
                                                height: 50,
                                                margin: EdgeInsets.all(5),
                                                child: ElevatedButton(
                                                    onPressed: () {},
                                                    child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                        children: [
                                                            Text('Krankheit einreichen'),
                                                            Icon(Icons.add)
                                                        ],
                                                    ),
                                                    style: ElevatedButton.styleFrom(
                                                        primary: Colors.black,
                                                        onPrimary: Colors.white,
                                                        textStyle: TextStyle(
                                                            fontSize: 15,
                                                            fontFamily: 'Roboto'
                                                        ),
                                                    ),
                                                ),
                                            ),
                                        ],
                                    ),
                                ),
                            ),
                            Card(
                                child: Container(
                                    width: double.infinity,
                                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                                    margin: EdgeInsets.only(bottom: 20),
                                    child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            titleHeader('AZ Konto'),
                                            ListTile(
                                                leading: Text(
                                                    'Stunden',
                                                    style: Theme.of(context).textTheme.headline6
                                                ),
                                                trailing: Text(
                                                    '100 / 250',
                                                    style: TextStyle(
                                                        fontFamily: 'AllertaStencil',
                                                        fontSize: 18,
                                                        fontWeight: FontWeight.bold
                                                    ),
                                                ),
                                            ),
                                        ],
                                    ),
                                ),
                            ),
                        ],
                    ),
                ),
            )
        );
    }
}