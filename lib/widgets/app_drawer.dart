import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// For personal imports
import '../models/navigation_model.dart';
import '../providers/navigation_provider.dart';
import '../screens/account_screen.dart';
import '../screens/tabs_screen.dart';
import '../screens/time_tracking_screen.dart';
import '../utils/header_banner.dart';

class AppDrawer extends StatefulWidget {
    @override
    _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
    Widget drawerTile(String url, String label, Navigation item, VoidCallback? onSelect) {
        final provider = Provider.of<NavigationProvider>(context);
        final currentItem = provider.navigationTile;
        final isSelected = item == currentItem;

        return Padding(
            padding: EdgeInsets.all(16),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: onSelect,
                        child: isSelected ? Container(
                            margin: EdgeInsets.only(top: 20, left: 5),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        width: 4,
                                        color: Colors.deepPurple.shade400
                                    )
                                ),
                            ),
                            child:  Column(
                                children: [
                                    Container(
                                        padding: EdgeInsets.all(5),
                                        child: Image.asset(
                                            url,
                                            color: Colors.black,
                                        ),
                                    ),
                                    Text(
                                        label,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'Roboto'
                                        )
                                    ),
                                    SizedBox(height: 5,)
                                ],
                            )
                        ) 
                        : Container(
                            margin: EdgeInsets.only(top: 20, left: 5),
                            child: Column(
                                children: [
                                    Container(
                                        padding: EdgeInsets.all(5),
                                        child: Image.asset(url),
                                    ),
                                    Text(
                                        label,
                                        style: TextStyle(
                                            color: Colors.grey.shade600,
                                            fontFamily: 'Roboto'
                                        )
                                    )
                                ],
                            ),
                        )
                    )
                ],
            )
        );
    }

    void selectedItem(BuildContext context, Navigation item){
        final provider = Provider.of<NavigationProvider>(context, listen: false);
        provider.setNavigationTile(item);

        switch(item) {
            case Navigation.meinkonto:
                Navigator.pushReplacementNamed(context, AccountScreen.routeName);
                break;
            case Navigation.visitenkarte:
                Navigator.pushReplacementNamed(context, TabsScreen.routeName);
                break;
            case Navigation.zeiterfassung:
                Navigator.pushReplacementNamed(context, TimeTrackingScreen.routeName);
                break;
            case Navigation.meineeinsatze:
                break;
        }
    }

    @override
    Widget build(BuildContext context) {
        return Drawer(
            child: Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                child: ListView(
                    children: [
                        Column(
                            children: [
                                headerBanner(context),
                                drawerTile(
                                    './assets/images/mein-konto.png', 
                                    'Mein Konto', 
                                    Navigation.meinkonto, 
                                    () => selectedItem(context, Navigation.meinkonto)
                                ),
                                drawerTile(
                                    './assets/images/visitenkarte.png', 
                                    'Visitenkarte', 
                                    Navigation.visitenkarte, 
                                    () => selectedItem(context, Navigation.visitenkarte)
                                ),
                                drawerTile(
                                    './assets/images/zeiterfassung.png', 
                                    'Zeiterfassung', 
                                    Navigation.zeiterfassung, 
                                    () => selectedItem(context, Navigation.zeiterfassung)
                                ),
                                drawerTile(
                                    './assets/images/meine-einsatze.png', 
                                    'Meine Einsätze', 
                                    Navigation.meineeinsatze, 
                                    () => selectedItem(context, Navigation.meineeinsatze)
                                ),
                            ],
                        ),
                    ],
                )
            ),
        );
    }
}