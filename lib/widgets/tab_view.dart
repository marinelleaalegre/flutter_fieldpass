import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

// For personal imports
import '../utils/title_header.dart';

class TabView extends StatelessWidget {

    Widget addTabView(String name, String email, String image, String qr, String position) {
        return SingleChildScrollView(
            child: Center(
                child: Card(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                            titleHeader('Visitenkarte'),
                            ListTile(
                                leading: CircleAvatar(
                                    radius: 30,
                                    child: Container(
                                        child: Image.asset(
                                            image,
                                        ),
                                    )
                                ),
                                title: Text(
                                    name,
                                    style: TextStyle(
                                        fontFamily: 'AllertaStencil',
                                        fontSize: 20,
                                    ),
                                ),
                                subtitle: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                        Text(
                                            email,
                                            style: TextStyle(
                                                fontFamily: 'Mulish',
                                                fontSize: 15,
                                                color: Colors.grey.shade600
                                            ),
                                        ),
                                        Container(
                                            padding: EdgeInsets.only(top: 5),
                                            child: Text(
                                                position,
                                                style: TextStyle(
                                                    fontFamily: 'Mulish',
                                                    fontSize: 15,
                                                    color: Colors.grey.shade600
                                                ),
                                            ),
                                        ),
                                    ],
                                )
                            ),
                            SizedBox(height: 10,),
                            Center(
                                child: Container(
                                    padding: EdgeInsets.all(20),
                                    child: Image.asset(
                                        qr,
                                        height: 250,
                                    ),
                                )
                            ),
                            SizedBox(height: 10,),
                            titleHeader('Adresse'),
                            Container(
                                padding: EdgeInsets.fromLTRB(20, 2, 30, 2),
                                margin: EdgeInsets.all(5),
                                child: ListTile(
                                    leading: Container(
                                        padding: EdgeInsets.only(right: 20),
                                        child: Image.asset('./assets/images/address.png'),
                                    ),
                                    title: Text(
                                        'Flutter Bootcamp',
                                        style: TextStyle(
                                            fontFamily: 'AllertaStencil',
                                            fontSize: 20,
                                        ),
                                    ),
                                    subtitle: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            Text(
                                                '6783 Ayala Ave',
                                                style: TextStyle(
                                                    fontFamily: 'Mulish',
                                                    fontSize: 15,
                                                    color: Colors.grey.shade600
                                                ),
                                            ),
                                            Text(
                                                '1200 Metro Manila',
                                                style: TextStyle(
                                                    fontFamily: 'Mulish',
                                                    fontSize: 15,
                                                    color: Colors.grey.shade600
                                                ),
                                            ),
                                        ],
                                    )
                                ),
                            ),
                            SizedBox(height: 10,),
                            titleHeader('Kontakt'),
                            Container(
                                padding: EdgeInsets.fromLTRB(10, 2, 30, 2),
                                margin: EdgeInsets.all(5),
                                width: double.infinity,
                                child: ListTile(
                                    leading: Container(
                                        padding: EdgeInsets.only(right: 20),
                                        child: Image.asset('./assets/images/user.png'),
                                    ),
                                    title: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                            Text(
                                                'T: +49 1234 56 789 01',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontFamily: 'AllertaStencil',
                                                ),
                                            ),
                                            Text(
                                                'F: +49 1234 56 789 01-2',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontFamily: 'AllertaStencil',
                                                ),
                                            ),
                                            Text(
                                                'M: +49 1234 56',
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontFamily: 'AllertaStencil',
                                                ),
                                            ),
                                            Text(
                                                'E:$email',
                                                style: TextStyle(
                                                    fontFamily: 'AllertaStencil',
                                                ),
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                            ),
                                        ],
                                    )
                                ),
                            ),
                            Center(
                                heightFactor: 3,
                                child: RichText(
                                    text: TextSpan(
                                        children: [
                                            TextSpan(
                                            text: 'www.flutter-bootcamp.com',
                                            style: new TextStyle(
                                                color: Colors.lightBlue.shade400,
                                                fontSize: 15.0,
                                                fontFamily: 'Mulish'
                                            ),
                                            recognizer: TapGestureRecognizer()
                                                    ..onTap = () { launch('https://flutter.dev/');
                                                },
                                            ),
                                        ],
                                    ),
                                ),
                            )
                        ],
                    ),
                )
            ),
        );
    }

    @override
    Widget build(BuildContext context) {
        return TabBarView(
            children: [
                addTabView('Greg Neu', 'max.mustermann@bkl.de', './assets/images/ava.png', './assets/images/Vector.png', 'Monteur'),
                addTabView('Andero Mustermann', 'andero.mustermann@f-bootcamp', './assets/images/ava.png', './assets/images/Vector.png', 'Abteilungsleiter')
            ]
        );
    }
}