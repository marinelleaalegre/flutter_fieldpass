# Getting started
---
- Clone the project from Bitbucket to your machine

    ```bash
    $ git clone <project_url>
    ```

- Navigate through the root directory of the project and open the `pubspec.yaml` file in your VS Code (IDE) and click `Ctrl+S` to install the packages
- To run the application, open `main.dart` file then click `Debug` or press `Ctrl+F5` and use the Google Pixel API 30 emulator

---

# Packages Installed

## [url_launcher 6.0.9](https://pub.dev/packages/url_launcher)

> A Flutter plugin for launching a URL. Supports iOS, Android, web, Windows, macOS, and Linux.

## [provider 6.0.0](https://pub.dev/packages/provider)

> A wrapper around InheritedWidget to make them easier to use and more reusable.

## [font_awesome_flutter 9.1.0](https://pub.dev/packages/font_awesome_flutter)

> The Font Awesome Icon pack available as set of Flutter Icons.

## [flutter_time_picker_spinner 2.0.0](https://pub.dev/packages/flutter_time_picker_spinner)

> Time Picker widget with spinner instead of a material time picker.

## [timetable 0.2.9](https://pub.dev/packages/timetable)

> Customizable, animated calendar widget including day & week views.

---

# UI Screenshots

| ***Login Page***  | ***App Drawer*** |
| --------  | ------------------- |
| ![LoginPage.jpg](assets/screenshots/LoginPage.jpg) | ![AppDrawer.jpg](assets/screenshots/AppDrawer.jpg)   | 

| ***My Account part 1***  | ***My Account part 2*** |
| --------  | ------------------- |
| ![MeinKontoPt1.jpg](assets/screenshots/MeinKontoPt1.jpg) | ![MeinKontoPt2.jpg](assets/screenshots/MeinKontoPt2.jpg)   | 

| ***Visiting Card Meine Visitenkarte tab part 1***  | ***Visiting Card Meine Visitenkarte tab part 2*** |
| --------  | ------------------- |
| ![VisitenkartePt1.jpg](assets/screenshots/VisitenkartePt1.jpg) | ![VisitenkartePt2.jpg](assets/screenshots/VisitenkartePt2.jpg)   | 

| ***Visiting Card Vorgesetzte tab part 1***  | ***Visiting Card Vorgesetzte tab part 2*** |
| --------  | ------------------- |
| ![VisitenkartePt3.jpg](assets/screenshots/VisitenkartePt3.jpg) | ![VisitenkartePt4.jpg](assets/screenshots/VisitenkartePt4.jpg)   | 

| ***Zeiterfussung***  |
| --------  |
| ![Zeiterfassung.jpg](assets/screenshots/Zeiterfassung.jpg) |

| ***Add Zeiterfussung Arbeitszeit tab part 1***  | ***Add Zeiterfussung Arbeitszeit tab part 2*** |
| --------  | ------------------- |
| ![AddZeiterfassungPt1.jpg](assets/screenshots/AddZeiterfassungPt1.jpg) | ![AddZeiterfassungPt2.jpg](assets/screenshots/AddZeiterfassungPt2.jpg)      | 

| ***Add Zeiterfussung Pause tab part 1***  | ***Add Zeiterfussung Pause tab part 2*** |
| --------  | ------------------- |
| ![AddZeiterfassungPt3.jpg](assets/screenshots/AddZeiterfassungPt3.jpg) | ![AddZeiterfassungPt4.jpg](assets/screenshots/AddZeiterfassungPt4.jpg)      | 